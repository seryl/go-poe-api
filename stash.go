package api

import (
	"encoding/json"

	"bitbucket.org/seryl/poe-api/item"
)

// StashTabsPath is the base URL for all stashtabs requests
// Note: They include a slash here for whatever reason
var StashTabsPath = "public-stash-tabs/"

// StashTab returns a single stash tab response
func (c *Client) StashTab(id string) (*StashTabResponse, error) {
	sTabResp := &StashTabResponse{}

	resp, err := c.Get(StashTabsPath + "?id=" + id)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(sTabResp)
	return sTabResp, err
}

// StashTabResponse is a the response wrapper around the stash api
type StashTabResponse struct {
	NextChangeID string      `json:"next_change_id"`
	Stashes      []*StashTab `json:"stashes"`
}

// StashTab is a single stashtab wrapper
type StashTab struct {
	Account       string `json:"accountName"`
	LastCharacter string `json:"lastCharacterName"`
	ID            string `json:"id"`
	Name          string `json:"stash"`
	Type          string `json:"stashType"`
	Items         []*StashItem
	Public        bool `json:"public"`
}

// StashItem represents a single item in the stash
type StashItem struct {
	// Positional Info
	SlotWidth  int `json:"w"`
	SlotHeight int `json:"h"`
	XLoc       int `json:"x"`
	YLoc       int `json:"y"`

	// State
	Verified          bool `json:"verified"`
	Identified        bool `json:"identified"`
	Corrupted         bool `json:"corrupted"`
	LockedToCharacter bool `json:"lockToCharacter"`
	Duplicated        bool `json:"duplicated"`
	Support           bool `json:"support"`
	IsRelic           bool `json:"isRelic"`

	// Item Info
	InventoryID string `json:"inventoryId"`
	ID          string `json:"id"`
	League      string `json:"league"`
	ItemLevel   int    `json:"ilvl"`

	Name string `json:"name"`
	Type string `json:"typeLine"`

	// Visual
	Icon         string   `json:"icon"`
	FrameType    int      `json:"frameType"`
	ArtFilename  string   `json:"artFilename"`
	CosmeticMods []string `json:"cosmeticMods"`

	Note              string   `json:"note"`
	Description       string   `json:"descrText"`
	SecondDescription string   `json:"secDescrText"`
	FlavourText       []string `json:"flavourText"`

	ProphecyText           string `json:"prophecyText"`
	ProphecyDifficultyText string `json:"prophecyDiffText"`

	TalismanTier int `json:"talismanTier"`

	// Sockets
	Sockets       []*item.Socket `json:"sockets"`
	SocketedItems []*StashItem   `json:"socketedItems"`

	// Properties
	Properties           []*item.Property
	AdditionalProperties []*item.Property

	Requirements          []*item.Property `json:"requirements"`
	NextLevelRequirements []*item.Property `json:"nextLevelRequirements"`

	ExplicitMods     []string `json:"explicitMods"`
	ImplicitMods     []string `json:"implicitMods"`
	EnchantMods      []string `json:"enchantMods"`
	CraftedMods      []string `json:"craftedMods"`
	FlaskUtilityMods []string `json:"utilityMods"`

	MaxStackSize int `json:"maxStackSize"`
	StackSize    int `json:"stackSize"`
}
