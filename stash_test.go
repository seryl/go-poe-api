package api_test

import (
	"reflect"
	"testing"

	"bitbucket.org/seryl/poe-api"
)

var expectedSimpleStashTabs = []*api.StashTab{
	&api.StashTab{
		Account:       "Nightlines",
		LastCharacter: "ProbablyKB",
		ID:            "a9a42a5dbda657f71b077ecd0692acce8d1d29c7dff3437e5ed8708f6cb8838f",
		Name:          "LEVELING UNIQUES",
		Type:          "PremiumStash",
		Items:         []*api.StashItem{},
		Public:        false,
	},
	&api.StashTab{
		Account:       "Jephire",
		LastCharacter: "",
		ID:            "3c8f1dc99dabab14f888755f133dfb532dee09edb2058dd7138d4409c6923f72",
		Name:          "$",
		Type:          "PremiumStash",
		Items:         []*api.StashItem{},
		Public:        false,
	},
}

func TestSimpleStashTabDecode(t *testing.T) {
	resp := &api.StashTabResponse{}
	err := GetJSONFixture("simple_stash_tabs.json", resp)
	if err != nil {
		t.Fatal(err)
	}

	if resp.NextChangeID != "2300-4354-3305-4370-1300" {
		t.Fatal("Next Change ID does not match")
	}

	for i, stash := range resp.Stashes {
		if !reflect.DeepEqual(stash, expectedSimpleStashTabs[i]) {
			t.Logf("Received: %+v", stash)
			t.Logf("Expected: %+v", expectedSimpleStashTabs[i])
			t.Fatal("Simple Stash Tabs decoding is not returning expected values")
		}
	}
}

func TestSingleItemDecode(t *testing.T) {
	resp := &api.StashItem{}
	err := GetJSONFixture("single_stash_item.json", resp)
	if err != nil {
		t.Fatal(err)
	}

	if resp.ItemLevel != 67 {
		t.Fatal("Failed parsing item level")
	}

	if resp.League != "Hardcore" {
		t.Fatal("Failed parsing league")
	}

	if resp.Type != "Mosaic Kite Shield" {
		t.Fatal("Unable to parse item type")
	}

	if len(resp.Properties) < 3 {
		t.Fatal("Item parsing is missing properties")
	}

	if resp.Properties[0].Name != "Chance to Block" {
		t.Fatal("Properties are not being decoded properly inside of a single item")
	}

	if len(resp.Requirements) < 3 {
		t.Fatal("Item parsing is missing requirements")
	}

	if resp.Requirements[0].Name != "Level" {
		t.Fatal("Requirements are not being decoded properly inside of a single item")
	}

	if len(resp.Requirements[0].Values) < 1 {
		t.Fatal("Values for requirements are not being parsed")
	}

	if resp.Requirements[0].Values[0].Data != "66" {
		t.Fatal("Example level requirements for a item are not being parsed")
	}

	if len(resp.SocketedItems) < 3 {
		t.Fatal("Failing to properly parse socketed items")
	}
}

func TestStashTabResponse(t *testing.T) {
	resp := &api.StashTabResponse{}
	err := GetJSONFixture("stash_tab_response.json", resp)
	if err != nil {
		t.Fatal(err)
	}

	if len(resp.Stashes) != 1 {
		t.Fatal("Unable to parse a full stash tab response")
	}
}
