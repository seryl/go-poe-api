.PHONY: bin test testv
binfile := $(shell ls ./cmd/)
buildfiles := $(shell glide novendor)

all: test bin

test:
	go test $(buildfiles)

testv:
	go test -v $(buildfiles)

bin:
	@mkdir -p bin
	@for dir in $(binfile); do \
		pushd cmd/$$dir && go build -o ../../bin/$$dir ./...; popd; \
	done

clean:
	@rm -rf ./bin

default: test
