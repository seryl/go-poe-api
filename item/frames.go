package item

// FrameType is outer frame color/picture for a given item
type FrameType int

// The enumerated available frametypes
const (
	NormalFrame FrameType = iota
	MagicFrame
	RareFrame
	UniqueFrame
	GemFrame
	CurrencyFrame
	DivinationCardFrame
	QuestItemFrame
	ProphecyFrame
	RelicFrame
)
