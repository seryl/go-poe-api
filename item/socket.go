package item

// Socket represents information for a single socket on a given item
type Socket struct {
	Group     int    `json:"group"`
	Attribute string `json:"attr"`
}

// AttributeName takes in an attribute abbreviation and returns the
// full name for the representation of the socket
func (s *Socket) AttributeName() string {
	switch s.Attribute {
	case "S":
		return "Strength"
	case "I":
		return "Intelligence"
	case "D":
		return "Dexterity"
	case "G":
		return "White"
	default:
		return ""
	}
}
