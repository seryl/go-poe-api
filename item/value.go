package item

import (
	"fmt"
	"strconv"
)

// ValueType represents the item/damage type of a value stat
type ValueType int

// Item Value Types
const (
	WhitePhysical ValueType = iota
	BlueModified
	Unknown2
	Unknown3
	Fire
	Cold
	Lightning
	Chaos
)

// Value is the struct representation of the value/pair types for an item
type Value struct {
	Data string
	Type ValueType
}

// ValueFromArray takes a itemvalue tuple and returns an itemvalue struct
func ValueFromArray(ivList []interface{}) (*Value, error) {
	var valType int
	var err error

	if len(ivList) < 2 {
		return &Value{}, fmt.Errorf("ItemValue tuple is too small to create struct: %+v", ivList)
	}

	switch v := ivList[1].(type) {
	case string:
		valType, err = strconv.Atoi(v)
	case float64:
		valType = int(v)
	case int:
		valType = v
	}

	return &Value{
		Data: ivList[0].(string),
		Type: ValueType(valType),
	}, err
}
