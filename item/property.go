package item

import (
	"encoding/json"
)

// Property represents a single item's property values
// Note: Need to write values parser
type Property struct {
	Name        string          `json:"name"`
	DisplayMode int             `json:"displayMode"`
	RawValue    json.RawMessage `json:"values"`
	Values      []*Value
	Type        int     `json:"type,omitempty"`
	Progress    float64 `json:"progress,omitempty"`
}

// Used to avoid recursion
type property Property

// UnmarshalJSON is the helper function for unmarshalling a Property record
func (p *Property) UnmarshalJSON(b []byte) (err error) {
	var prop = property{}
	var ivList = [][]interface{}{}
	var valueList = []*Value{}
	var value = &Value{}

	if err = json.Unmarshal(b, &prop); err == nil {
		*p = Property(prop)
	} else {
		return
	}

	if err = json.Unmarshal(p.RawValue, &ivList); err == nil {
		for _, val := range ivList {
			value, err = ValueFromArray(val)
			if err != nil {
				return
			}

			valueList = append(valueList, value)
		}
	} else {
		return
	}

	p.Values = valueList
	return
}
