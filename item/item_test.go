package item_test

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path"
	"path/filepath"
	"runtime"
)

var RelFixtureDir = "../test/fixtures"

// FixtureDir attempts to find the fixture directory returning the absolute
// path and any errors of whether or not the path could be determined.
func FixtureDir() (string, error) {
	_, testFile, _, _ := runtime.Caller(1)
	absPath, err := filepath.Abs(path.Join(path.Dir(testFile), RelFixtureDir))
	if err != nil {
		return "", err
	}

	return absPath, nil
}

// GetFixture is a helper function to return an io.ReadCloser to a requested fixture.
// Note: it is the client's responsibility to Close the reader.
func GetFixture(filename string) (io.ReadCloser, error) {
	baseDir, err := FixtureDir()
	if err != nil {
		return nil, err
	}

	fixturePath := path.Join(baseDir, filename)
	_, err = os.Stat(fixturePath)
	if os.IsNotExist(err) {
		return nil, fmt.Errorf("Fixture does not exist at the requested path: `%s`", fixturePath)
	}

	if err != nil {
		return nil, err
	}

	return os.Open(fixturePath)
}

// GetJSONFixture is a helper function to read a requested json fixture
// and set the current target to the decoded output.
func GetJSONFixture(filename string, target interface{}) error {
	reader, err := GetFixture(filename)
	if err != nil {
		return err
	}
	defer reader.Close()

	return json.NewDecoder(reader).Decode(target)
}
