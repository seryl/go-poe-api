package item_test

import (
	"reflect"
	"testing"

	"bitbucket.org/seryl/poe-api/item"
)

var valueFromArrayCases = []struct {
	input    []interface{}
	expected *item.Value
}{
	{[]interface{}{"39499861/62460771", 0}, &item.Value{
		Data: "39499861/62460771",
		Type: item.WhitePhysical,
	}}, // experience

	{[]interface{}{"229", 1}, &item.Value{
		Data: "229",
		Type: item.BlueModified,
	}}, // armour

	{[]interface{}{"5-12", 4}, &item.Value{
		Data: "5-12",
		Type: item.Fire,
	}}, // elemental damage (fire)
}

func TestValueFromArray(t *testing.T) {
	for _, tst := range valueFromArrayCases {
		val, err := item.ValueFromArray(tst.input)
		if err != nil {
			t.Fatalf("Unable to parse itemvalue from input: `%+v`, err: %+v", tst.input, err)
		}

		if !reflect.DeepEqual(val, tst.expected) {
			t.Fatalf("Array tuples are not able to turn into struct: %+v", tst)
		}
	}
}
