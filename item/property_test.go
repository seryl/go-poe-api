package item_test

import (
	"testing"

	"bitbucket.org/seryl/poe-api/item"
)

func TestDecodeProperty(t *testing.T) {
	resp := &item.Property{}
	err := GetJSONFixture("property.json", resp)
	if err != nil {
		t.Fatal(err)
	}

	if resp.Name != "Experience" {
		t.Fatal("Failed to parse name for property")
	}

	if len(resp.Values) < 1 {
		t.Fatal("Not parsing values for properties")
	}

	if resp.Values[0].Data != "11781205/16159983" {
		t.Fatal("Failed parsing property child value data")
	}

	if resp.Values[0].Type != item.WhitePhysical {
		t.Fatal("Failed parsing property child value type")
	}

	if resp.DisplayMode != 2 {
		t.Fatal("Failed to parse DisplayMode for property")
	}

	if resp.Progress != 0.72903573513 {
		t.Fatal("Failed to parse Progress for property")
	}
}

func TestDecodePropertyList(t *testing.T) {
	resp := []*item.Property{}
	err := GetJSONFixture("property_list.json", &resp)
	if err != nil {
		t.Fatal(err)
	}

	if len(resp) < 5 {
		t.Fatal("Decoded properties.json does not match the quantity")
	}

	if resp[0].Name != "Curse, Spell, AoE, Duration" {
		t.Fatal("Unable to unmarshal first property name")
	}

	if len(resp[4].Values) != 1 {
		t.Fatal("Property values are not unmarshalling correctly")
	}

	if resp[4].Type != 6 {
		t.Fatal("Incorrectly parsing types for properties")
	}

	if resp[4].Values[0].Data != "+8%" {
		t.Fatalf("Incorrectly parsing data for child prop values")
	}

	if resp[4].Values[0].Type != item.BlueModified {
		t.Fatal("Incorrectly parsing type for child prop values")
	}
}
