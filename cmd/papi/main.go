package main

import (
	"fmt"

	"bitbucket.org/seryl/poe-api"
)

func main() {
	client := api.New()

	st, err := client.StashTab("220-1652-744-1341-230")
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Printf("%+v\n", st)
}
