package api

import (
	"net/http"
	"time"
)

// APIPath is the path of exile base url
var APIPath = "http://api.pathofexile.com/"

// Client is the base consumer for tha path of exile api
type Client struct {
	client  *http.Client
	Retries int
}

// New returns an api client
func New() *Client {
	return &Client{
		client:  &http.Client{Timeout: 10 * time.Second},
		Retries: 10,
	}
}

// Get is a retryable api request
// Note: It is the client's responsibility to close the response body
func (c *Client) Get(uri string) (*http.Response, error) {
	var err error
	var resp *http.Response

	req, err := http.NewRequest("GET", APIPath+"/"+uri, nil)
	if err != nil {
		return nil, err
	}

	for i := 0; i < c.Retries; i++ {
		resp, err = c.client.Do(req)
		if err == nil {
			if resp.StatusCode == 200 {
				break
			}
		}
	}

	return resp, err
}
